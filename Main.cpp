#include <iostream>

using namespace std;

class Animal

{

public:

virtual void Voice ()
	{
		cout << "Animal says: " << endl;
	}

};

class Dog : public Animal
{

public:

	void Voice() override
	{
		cout << "Wow Wow" << endl;
	}

	
};

class Cat : public Dog
{
public:
	void Voice() override
	{
		cout << "Meow" << endl;
	}
};

class Cow : public Cat
{
public:
	void Voice() override
	{
		cout << "Moo!" << endl;

	}
};




int main()
{
	const int Size = 3;
	
	Animal* array[Size] = { new Dog, new Cat, new Cow };
	
	for (int i=0; i<Size;i++)

{
		array[i]->Voice();
}
	
}